# D3 - Data Journalism and D3
![7-animated-scatter](images/7-animated-scatter.gif)

#### Correlations Discovered Between Health Risks and Age, Income
* Poverty Vs Uninsured:
    - Low-income households are more likely to be uninsured than people in higher income households.
*******************************

* Poverty Vs Smoking:
    - Generally, states with more resdents below the poverty level show higher rates of cigarette smoking. 
*******************************

* Poverty Vs Obesity:
    - Poverty-dense areas oftentimes have poor access to fresh and healthy food options which may explain higher obesity rates.
*******************************   
     
* Age Vs Uninsured:
    - Young adults are the most likely to go without coverage. 
    - Massachusetts has the lowest number of uninsured and Texas has the highest rate but with a much lower median age.
*******************************  
        
* Age Vs Smoking:
    - States with older age groups tend to have the highest numbers of smokers. 
    - In states where the median age is younger much of the population doesn't smoke.
*******************************

* Age Vs Obesity:
    - There may be a slight correlation between the median age and obesity. 
    - Higher age groups seam more succeptible to being obese. 
    - This is probably due to them being generally less active than younger groups.
    - However, other factors like poverty and income may also affect obesity levels.
*******************************

* Income Vs Uninsured:
    - Most uninsured people are in low-income households. 
    - The lack of public coverage in states like Texas leads to a higher uninsured rates.
*******************************

* Income Vs Smoking:
    - Similar to states with high poverty, states with lower median household income will also tend to have higher rates of smoking. 
    - States with higher incomes have some of the lowest rates of smoking.
*******************************

* Income Vs Obesity:
    - Levels of obesity tend to decrease with increased levels of income. 
    - Lower income populations don't always have the same access to healthy food choices as higher income individuals.
				
			
		

